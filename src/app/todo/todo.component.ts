import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-todo",
  templateUrl: "./todo.component.html",
  styleUrls: ["./todo.component.scss"]
})
export class TodoComponent implements OnInit {

  description: string;
  status: string;
  todoItem: any[] = [];
  todo: {};
  myTodo: any;
  constructor() { }

  ngOnInit() {

  }

  createTodo() {
    this.todo = {
      "desc": this.description,
      "status": this.status
    }
    this.todoItem.push(this.todo);
    localStorage.setItem("todo", JSON.stringify(this.todoItem));
    this.loadTable();
  }

  editTodo() {

  }

  viewTodo() {

  }

  removeTodo() {

  }

  loadTable() {
    this.myTodo = JSON.parse(localStorage.getItem("todo"));
  }

  clearTodo() {
    localStorage.clear();
  }
}
