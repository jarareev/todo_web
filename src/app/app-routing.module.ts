import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TodoComponent } from "./todo/todo.component";
import { HomeComponent } from "./home/home.component";
const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "todo",
    component: TodoComponent
  },
  // otherwise redirect to home
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
